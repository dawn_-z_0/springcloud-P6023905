package com.redis.producer.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;


@RestController
public class RedisDemo {
    @Resource
    RedisTemplate redisTemplate;

    // redis键
    String key = "myconfig";

    @GetMapping("/producer")
    public String providder() {
        // redis值
        String value = "this is my config";
        //生产内容
        redisTemplate.opsForValue().set(key, value);

        return "生产成功key:" + key;
    }
}
