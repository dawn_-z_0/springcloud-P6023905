package com.redis.consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
public class RedisDemo {
    @Autowired
    private RedisTemplate<String, String> redisTemplateString;

    @Resource
    RedisTemplate redisTemplate;

    String key = "myconfig";

    @GetMapping("/consumer")
    public String consumer() {
        //获取key值
        Object value = redisTemplate.opsForValue().get(key);
        if (value == null) {
            return "没有东西可消费";
        }
        return "正在消费……>>>>>>>>【" + key + "】-----" + value;
    }

    @GetMapping("redis/setValue")
    public String setValue() {
        String key_test = "Chinese";
        String value = "china";

        redisTemplateString.opsForValue().set(key_test, value);
        return "Redis添加key：" + key_test + ",value:" + value;
    }
}
