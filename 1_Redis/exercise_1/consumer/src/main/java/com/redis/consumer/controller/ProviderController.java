package com.redis.consumer.controller;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.CountDownLatch2;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ProviderController {

    @GetMapping("/syncMethod")
    public void method1() throws Exception {
        //实例化消息生产者Producer
        DefaultMQProducer producer = new DefaultMQProducer("provider");
        //设置NameServer的地址及端口
        producer.setNamesrvAddr("127.0.0.1:9876");
        //设置超时时间
        producer.setSendMsgTimeout(15000);
        //启动Producer实例
        producer.start();
        for (int i = 0; i < 10; i++) {
            Message msg = new Message("TopicTest", "TagA", ("Hello RocketMQ [" + i + "]").getBytes());
            SendResult sendResult = producer.send(msg);
            System.out.println("Rocket MQ" + sendResult);
        }
        //关闭实例
        producer.shutdown();
    }


    @GetMapping("/Consumer")
    public void Consumer() throws Exception {
        //实例化消息消费者
        DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer();
        //设置NameServer的地址及端口
        defaultMQPushConsumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅一个或多个Topic 以及tag来过滤消息
        defaultMQPushConsumer.subscribe("TopicTest", "*");
        //注册回调实现类，来处理从broker拉取回来的消息
        defaultMQPushConsumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                System.out.println(Thread.currentThread().getName() + "----:" + list);
                //标记消息已经被成功消费
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        System.out.println("调用消费者方法完成！");

    }

}
