package com.redis.distributed01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Distributed01Application {

    public static void main(String[] args) {
        SpringApplication.run(Distributed01Application.class, args);
    }

}
