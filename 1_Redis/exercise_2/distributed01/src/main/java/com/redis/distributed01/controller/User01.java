package com.redis.distributed01.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;


@RestController
public class User01 {
    @Resource
    RedisTemplate redisTemplate;
    //分布式锁键,实现竞争
    final String LOCK_KEY01 = "mylock01";
    //分布式锁键，实现占用与释放
    final String LOCK_KEY02 = "mylock02";

    /**
     * 实现竞争
     * setIfAbsent()如果为空就set值，并返回1
     * 如果已存在则不进行操作，并返回0
     */
    @GetMapping("/have")
    public String haveLock() {
        boolean flag = redisTemplate.opsForValue().setIfAbsent(LOCK_KEY01, "user01");
        if (flag) {
            return "竞争成功";
        }
        //已有别的用户设置成功
        return "竞争失败";
    }

    /**
     * 占用锁，时间到就释放锁
     *
     * @author mobiusband
     */
    @GetMapping("/time")
    public String timeLock() {
        return getLock();
    }

    /**
     * 占用锁并写入自己用户名，设置过期时间；
     * 判断键值是否属于自己，若属于则显示正在占用，若不属于则等待
     */
    public String getLock() {
        boolean flag = redisTemplate.opsForValue().setIfAbsent(LOCK_KEY02, "user01", 5, TimeUnit.SECONDS);
        if (flag) {
            System.out.println("抢占成功，5S后释放");
            if (redisTemplate.opsForValue().get(LOCK_KEY02).equals("user01")) {
                return "正在占用";
            }
            return "等待锁释放。。。";
        } else {
            return "已被别的用户抢占。。。";
        }

    }
}
