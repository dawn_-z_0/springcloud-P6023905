package com.redis.distributed02;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Distributed02Application {

    public static void main(String[] args) {
        SpringApplication.run(Distributed02Application.class, args);
    }

}
