package com.redis.distributed02.controller;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;


@RestController
public class User02 {

    @Resource
    RedisTemplate redisTemplate;
    //分布式锁键,实现竞争
    final String LOCK_KEY01 = "mylock01";
    //分布式锁键，实现占用与释放
    final String LOCK_KEY02 = "mylock02";

    /**
     * 实现竞争
     */
    @GetMapping("/have")
    public String haveLock() {
        boolean flag = redisTemplate.opsForValue().setIfAbsent(LOCK_KEY01, "user02");
        if (flag) {
            return "竞争成功";
        }
        return "竞争失败";
    }

    @GetMapping("/time")
    public String timeLock() {
        return getLock();
    }

    /**
     * 占用锁并写入自己用户名，设置过期时间；
     * 判断键值是否属于自己，若属于则显示正在占用，若不属于则等待
     */
    public String getLock() {
        boolean flag = redisTemplate.opsForValue().setIfAbsent(LOCK_KEY02, "user02", 10, TimeUnit.SECONDS);
        if (flag) {
            //若true，则代表抢占成功,此处意义不大
            if (redisTemplate.opsForValue().get(LOCK_KEY02).equals("user02")) {
                return "正在占用";
            }
            return "等待释放";
        } else {
            return "已被别的用户抢占,请等待释放";
        }
    }
}
