package com.gientech.producer.Controller;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
public class producer {

    @Resource
    private RocketMQTemplate rocketMQTemplate;

    @GetMapping("/syncMethod")
    public void method1() throws Exception {
        //实例化消息生产者Producer
        DefaultMQProducer producer = new DefaultMQProducer("provider");
        //设置NameServer的地址及端口
        producer.setNamesrvAddr("127.0.0.1:9876");
        //设置超时时间
        producer.setSendMsgTimeout(15000);
        //启动Producer实例
        producer.start();
        for (int i = 0; i < 10; i++) {
            Message msg = new Message("TopicTest", "TagA", ("Hello RocketMQ [" + i + "]").getBytes());
            SendResult sendResult = producer.send(msg);
            System.out.println("Rocket MQ" + sendResult);
        }
        //关闭实例
        producer.shutdown();
    }


    @RequestMapping("/push")
    public String get() {
        //生产消息
        rocketMQTemplate.convertAndSend("mytopic", "hello world");

        return "生产消息……";
    }

    //用户注册
    @RequestMapping("/newUser")
    public String userReg() {

        String user01 = "name: zhangsan, id:01, password: 123456";
        //发送消息到rocketmq，主题为user，信息为user01
        rocketMQTemplate.convertAndSend("user1", user01);

        return "注册成功";
    }
}
