package com.gientech.consumer.Controller;


import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//监听mytopic，实现简单消息传送  从此处进入
@Component
@RestController
@RocketMQMessageListener(topic = "mytopic", consumerGroup = "my-consumer-group")
public class Consumer implements RocketMQListener<String> {

    @GetMapping("/Consumer")
    public void Consumer() throws Exception {
        //实例化消息消费者
        DefaultMQPushConsumer defaultMQPushConsumer = new DefaultMQPushConsumer();
        //设置NameServer的地址及端口
        defaultMQPushConsumer.setNamesrvAddr("127.0.0.1:9876");
        //订阅一个或多个Topic 以及tag来过滤消息
        defaultMQPushConsumer.subscribe("TopicTest", "*");
        //注册回调实现类，来处理从broker拉取回来的消息
        defaultMQPushConsumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                System.out.println(Thread.currentThread().getName() + "----:" + list);
                //标记消息已经被成功消费
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        System.out.println("调用消费者方法完成！");

    }

    @Override
    //重写onMessage方法
    public void onMessage(String s) {
        //获取消息,打印日志
        System.out.println("Consumer消费者接收到信息：" + s);
    }

}
