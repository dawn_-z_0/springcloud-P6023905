package com.gientech.consumer.Controller;

import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
@Component
@RestController
//监听 user 获取到 user1的信息即可进行存储等
@RocketMQMessageListener(topic = "user1", consumerGroup = "my-consumer-group")
public class UserController implements RocketMQListener<String> {

    @Resource
    RedisTemplate redisTemplate;

    String user;
    //用户信息redis存储key
    String key = "userName";


    @Override
    public void onMessage(String s) {
        System.out.println("User@@@@@接收信息：" + s);
        //如果有信息则传入redis
        if (s != null && !"".equals(s)) {
            user = s;
            redisTemplate.opsForValue().set(key, s);
        }
    }


    @GetMapping("/getUser")
    public String getUser() {
        String value = redisTemplate.opsForValue().get(key).toString();
        if (value != null && !"".equals(value)) {
            return "从Redis缓存中获取用户信息:【" + key + "】：" + value;
        }else {
            return "未查询到相关信息，该用户未注册！";
        }
    }
}
