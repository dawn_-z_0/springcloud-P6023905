package com.gitentech.springexercise.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/test")
    public String test(){

        return "啦啦啦，我收到请求了！";
    }
}
