package com.example.Consumer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;


@RestController
public class ConsumerController {
    @Resource
    private RestTemplate restTemplate;

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/consumerTest")
    public String test() {
        List<ServiceInstance> instances = this.discoveryClient.getInstances("producerTest");
        ServiceInstance serviceInstance = instances.get(0);
        //获取地址
        URI uri = serviceInstance.getUri();
        String targetUrl = uri + "/producerTest";
        System.out.println("发现地址为：" + targetUrl);
        String rst = restTemplate.getForObject(targetUrl, String.class);

        return "消费者发现地址为: " + rst;
    }
}
