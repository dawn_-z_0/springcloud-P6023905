package com.example.Consumer.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ProducerController {

    @GetMapping("/producerTest")
    public String register(){
        return "服务注册：服务注册生产者测试";
    }
}
